<?php

namespace Avanti\PersonType\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Avanti\PersonType\Helper\Config;

class Data extends AbstractHelper
{

    public function isEnabled()
    {
        return $this->getConfigFlag(Config::CONFIG_ENABLED);
    }

    public function getConfigFlag($path)
    {
        return $this->scopeConfig->isSetFlag($path);
    }
}