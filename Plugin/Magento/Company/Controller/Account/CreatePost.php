<?php

namespace Avanti\PersonType\Plugin\Magento\Company\Controller\Account;

use Magento\Company\Controller\Account\CreatePost as CreatePostController;
use Avanti\PersonType\Helper\Data;

class CreatePost
{

    protected $helper;

    public function __construct(
        Data $helper
    ) {
        $this->helper = $helper;
    }

    public function beforeExecute(CreatePostController $subject)
    {
        if ($this->helper->isEnabled()) {
            $customerData = $subject->getRequest()->getParams();

            if (!isset($customerData['company']['legal_name'])) {
                $customerData['company']['legal_name'] = $customerData['company']['company_name'];
            }

            if (!isset($customerData['email'])) {
                $customerData['email'] = $customerData['company']['company_email'];
            }

            if (!isset($customerData['firstname'])) {
                $splitFullName = explode(' ', $customerData['company']['company_name'], 2);

                if (count($splitFullName) > 1) {
                    $customerData['firstname'] = $splitFullName[0] ? $splitFullName[0] : "";
                    $customerData['lastname'] = $splitFullName[1] ? $splitFullName[1] : "";
                } else {
                    $customerData['firstname'] = " ";
                }

            }

            $subject->getRequest()->setParams($customerData);
        }

        return [$subject];
    }
}