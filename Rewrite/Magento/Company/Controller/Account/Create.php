<?php

namespace Avanti\PersonType\Rewrite\Magento\Company\Controller\Account;

use Magento\Framework\Controller\ResultFactory;
use Magento\Company\Controller\Account\Create as CreateController;

class Create extends CreateController
{
    public function execute()
    {
        $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $result->getConfig()->getTitle()->set(__('Create Account'));
        return $result;
    }
}